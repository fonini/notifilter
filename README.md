# Notifilter

Notifications filter for HexChat.


## Config

If you don't have a config file, default parameters will be used.

If you want to write a config file, it should be named `config`, and
placed in `${XDG_CONFIG_HOME}/notifilter/config`. It should have up to
three lines:

1.  Number of seconds to wait, starting when a user has last talked,
    before he is declared inactive. Inactive users don't generate
    join/part/quit messages. By default, this is 6 hours (i.e. `21600`).
2.  Either the string `debug` or some other string. (May be empty.) If
    the string is `debug`, messages which would be suppressed are
    instead shown in gray. By default, debug mode is not used.
3.  A comma-separated list of nicknames which should be ignored by
    Notifilter. When a nickname is ignored, his join/part/quit messages
    are never suppressed.

If just some of these three lines are specified, the default is used for
the rest of them.

Examples:

```
10000
debug
mynickname
```

```
600
# debug
mynickname1,mynickname2
```


## Installation

```bash
cmake --preset release
cmake --build --preset release
cp build/Release/notifilter.so ~/.config/hexchat/addons/
```
