#[================================================================[.md:
FindHexChatPlugin
=================

Finds the HexChat plugin headers.


Imported Targets
----------------

This module provides the `HexChatPlugin::HexChatPlugin` imported
target, if found.


Result Variables
----------------

This will define the following variables:

* `HexChatPlugin_FOUND`:
  True if the system has the HexChat plugin headers.
* `HexChatPlugin_VERSION`:
  The version of the HexChat distribution providing the headers.


Cache Variables
---------------

The following cache variables may also be set:

* (Marked as "advanced") `HexChatPlugin_INCLUDE_DIR`:
  The directory containing `hexchat-plugin.h`.
#]================================================================]

find_package(PkgConfig)
pkg_check_modules(PC_HexChatPlugin QUIET hexchat-plugin)

find_path(HexChatPlugin_INCLUDE_DIR
  hexchat-plugin.h
  HINTS ${PC_HexChatPlugin_INCLUDE_DIRS} ${PC_HexChatPlugin_INCLUDEDIR}
  PATHS /usr/include /usr/local/include
  DOC "Path to hexchat-plugin.h"
)

set(HexChatPlugin_VERSION ${PC_HexChatPlugin_VERSION})

include(FindPackageHandleStandardArgs)
set(_fhcp_failmsg "ACTION REQUIRED:
Compiling this module requires the ‘hexchat-plugin.h’ header, but it \
looks like it couldn't be found in any standard system path. You need \
to place it at ‘/usr/local/include’ and then reconfigure the project.
This header should be somewhere on your system if you have HexChat \
installed. If you can't find it for any reason, download it from \
GitHub:
    https://github.com/hexchat/hexchat/blob/master/src/common/hexchat-plugin.h"
)
find_package_handle_standard_args(HexChatPlugin
  FOUND_VAR HexChatPlugin_FOUND
  REQUIRED_VARS HexChatPlugin_INCLUDE_DIR
  VERSION_VAR HexChatPlugin_VERSION
  REASON_FAILURE_MESSAGE "${_fhcp_failmsg}"
)

if(HexChatPlugin_FOUND AND NOT TARGET HexChatPlugin::HexChatPlugin)
  add_library(HexChatPlugin::HexChatPlugin INTERFACE IMPORTED)
  set_target_properties(HexChatPlugin::HexChatPlugin PROPERTIES
    INTERFACE_INCLUDE_DIRECTORIES "${HexChatPlugin_INCLUDE_DIR}"
    INTERFACE_LINK_OPTIONS "-Wl,--export-dynamic"  # recommended by HexChat
  )
endif()

mark_as_advanced(HexChatPlugin_INCLUDE_DIR)
