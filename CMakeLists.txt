cmake_minimum_required(VERSION 3.24)
project(Notifilter
  VERSION 0.1.0
  DESCRIPTION "Notification filter for HexChat"
  LANGUAGES CXX
)

list(APPEND CMAKE_MODULE_PATH "${PROJECT_SOURCE_DIR}/cmake")


# Build options

set(NOTIFILTER_CONFIG_DEFAULT_DIR ".config" CACHE STRING
"User configutarions directory (relative to HOME) to be used in case \
XDG_CONFIG_GOME is not set.")

set(NOTIFILTER_CONFIG_PREFER_ENV ON CACHE BOOL
"In case XDG_CONFIG_HOME is set, use it and ignore \
NOTIFILTER_CONFIG_DEFAULT_DIR.")

set(NOTIFILTER_DEFAULT_USER_ACTIVE_THRESHOLD_SECONDS "6 * 3600" CACHE STRING
"Default value for the 'user active threshold' setting, in seconds.")

configure_file(
  "${PROJECT_SOURCE_DIR}/include/cmake_defines.h.in"
  "${PROJECT_BINARY_DIR}/include/cmake_defines.h"
  @ONLY
)


# Setup target: module notifilter.so

find_package(HexChatPlugin 2.14.3 REQUIRED)

add_library(notifilter MODULE
  src/config.cpp
  src/debug.cpp
  src/io.cpp
  src/notifilter.cpp
  src/plugin.cpp
)

target_include_directories(notifilter PRIVATE "${PROJECT_BINARY_DIR}/include")
target_compile_features(notifilter PRIVATE cxx_std_17)
set_target_properties(notifilter PROPERTIES
  CXX_STANDARD_REQUIRED ON
  CXX_EXTENSIONS OFF  # -std=c++17 instead of gnu++17
  INTERPROCEDURAL_OPTIMIZATION ON
  PREFIX ""
)

# Compilation setup

set(NOTIFILTERCF  # Notifilter Compilation flags
  -Wall -Wextra -pedantic -mtune=native -march=native
  -fvisibility=hidden -fno-semantic-interposition
  $<$<CONFIG:Debug>:-Og -fno-inline -ggdb3>
  $<$<CONFIG:Release>:-O3>
  $<$<CONFIG:RelWithDebInfo>:-O3 -ggdb3>)
target_compile_options(notifilter PRIVATE ${NOTIFILTERCF})
target_link_options(notifilter PRIVATE ${NOTIFILTERCF})

target_link_libraries(notifilter PRIVATE HexChatPlugin::HexChatPlugin)
