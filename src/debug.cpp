#include "debug.h"

#include <cstdint>

#include <hexchat-plugin.h>

#include "config.h"
#include "data.h"
#include "notifilter.h"

int
notifilterdebug_cb(
    char ** /*word*/,
    char ** /*word_eol*/,
    void * /*userdata*/
) {
    hexchat_printf(ph, "========  %s  v%s  ========", PNAME, PVERSION);
    hexchat_printf(
        ph, "config::data.user_active_threshold_seconds = %d",
        config::data.get_user_active_threshold());
    hexchat_printf(
        ph, "config::data.debug = %d",
        config::data.is_debug());
    hexchat_printf(
        ph, "config::data.ignore_nicks = %s",
        config::data.get_ignore_nicks_buffer());
    hexchat_print(ph, "");

    hexchat_print(ph, "active_users.ignored_nicks =");
    for (const auto & nick : active_users->get_ignored_nicks())
        { hexchat_printf(ph, "    %s", nick.c_str()); }
    hexchat_print(ph, "active_users.data =");
    for (const auto & [key, val] : active_users->get_data()) {
        hexchat_printf(
            ph, "    %s::%s: %jd",
            key.first.c_str(),
            key.second.c_str(),
            static_cast<std::intmax_t>(val)
        );
    }

    hexchat_print(ph, "======================================");

    return HEXCHAT_EAT_ALL;
}
