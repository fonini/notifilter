#ifndef NOTIFILTER_IO_H_
#define NOTIFILTER_IO_H_

#include <filesystem>
#include <fstream>
#include <stdexcept>
#include <string>


namespace io {

class LinesFileReader {
    class Line;

public:
    explicit LinesFileReader(const std::filesystem::path & filepath);

    static Line end() { return {}; }

    Line begin() {
        Line line {this};
        return ++line;
    }

    /** Reads a line, returning `false` it there is none.
     *
     *  If there are still unread line in the stream, reads the next
     *  line to the parameter `line`, and returns true. Otherwise,
     *  doesn't touch `line` and returns `false`.
     */
    bool getline(std::string & line);

private:
    class Line {
    public:
        Line()
          : reader{nullptr}
        {}
        explicit Line(LinesFileReader * ptr)
          : reader{ptr}
        {}

        bool operator !=(const Line & rhs) const
            { return (!reader) != (!rhs.reader); }

        Line & operator ++() {
            if (!reader->getline(str))
                { reader = nullptr; }
            return *this;
        }

        std::string & operator *() { return str; }
        const std::string & operator *() const { return str; }

    private:
        std::string str;
        LinesFileReader * reader;
    };

    /** Reads a `char` from the stream, returns false on EOF.
     *
     *  Attempts to read a `char`. If there is an error (i.e., either a
     *  badbit or else a failbit without an eofbit), throws
     *  `std::ios_base::failure`. If it hits EOF without a proper error,
     *  it maintains the eofbit and returns `false`.
     */
    bool getchar(char & ch);

    std::ifstream input;
};


class Error: public std::runtime_error
    { using std::runtime_error::runtime_error; };

}  // namespace io

#endif  // NOTIFILTER_IO_H_
