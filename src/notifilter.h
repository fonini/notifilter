#ifndef NOTIFILTER_NOTIFILTER_H_
#define NOTIFILTER_NOTIFILTER_H_

#include <cstddef>

#include <filesystem>
#include <ios>
#include <new>
#include <stdexcept>

#include <hexchat-plugin.h>

#include "config.h"
#include "io.h"


inline hexchat_plugin * ph = nullptr;


// These are not 'const' because the HexChat interface requires them
// (probably by mistake) not to be.
inline char PNAME[] = "Notifilter";
inline char PDESC[] = "Notifications filter";
inline char PVERSION[] = "0.1.0";


constexpr inline std::size_t errmsg_buffer_size = 1024;

/** Buffer for error messages to be accessible from outside the `catch`
 *  block.
 */
inline char current_error_msg[errmsg_buffer_size] = "";

[[gnu::format(printf, 1, 2)]]
int set_new_error_msg(const char * fmt...) noexcept;

inline void reset_error_msg() noexcept {
    current_error_msg[0] = '\0';
}


/** Executes `f` inside a try-catch block, and catch all exceptions the
 *  code could possibly throw. In case an exception is intecepted, the
 *  `set_new_error_msg` function is used to save the exception data.
 *
 *  How to use:
 *
 *  safe_exec([]{
 *      if (!try_dangerous_stuff())
 *          throw SomeError{};
 *  });
 *  if (current_error_msg[0] != '\0') {
 *      // ... deal with the error ...
 *      reset_error_msg();
 *  }
 *  else {
 *      // there was no error
 *  }
 */
template<typename Func>
void safe_exec(Func f) noexcept {
    try { f(); }
    catch (const std::length_error & e) {
        set_new_error_msg(
            "String or path with unsupported length: %s",
            e.what());
    }
    catch (const std::ios_base::failure & e) {
        set_new_error_msg(
            "File read error: %s",
            e.what());
    }
    catch (const std::filesystem::filesystem_error & e) {
        set_new_error_msg(
            "Filesystem error: %s",
            e.what());
    }
    catch (const std::bad_alloc & e) {
        set_new_error_msg(
            "Out of memory: %s",
            e.what());
    }
    catch (const io::Error & e) {
        set_new_error_msg(
            "File read error: %s",
            e.what());
    }
    catch (const config::InvalidFilesystemEnvironment & e) {
        set_new_error_msg(
            "%s: Error reading the configuration: %s",
            e.get_path().c_str(), e.what());
    }
}

#endif  // NOTIFILTER_NOTIFILTER_H_
