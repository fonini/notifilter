#ifndef NOTIFILTER_DATA_H_
#define NOTIFILTER_DATA_H_

#include <ctime>

#include <algorithm>
#include <iterator>
#include <map>
#include <sstream>
#include <string>
#include <utility>
#include <vector>

#include <hexchat-plugin.h>

#include "config.h"
#include "notifilter.h"


namespace gsl { template <typename T> using owner = T; }


namespace utils {
// string splitting: https://stackoverflow.com/q/236129
template <typename Out>
void split_string(const char * str, char delim, Out result) {
    std::istringstream iss{str};
    std::string item;
    while (std::getline(iss, item, delim))
        { *result++ = item; }
}

inline std::vector<std::string>
split_string(const char * str, char delim) {
    std::vector<std::string> items;
    split_string(str, delim, std::back_insert_iterator{items});
    return items;
}
}


class ActiveUsers {
public:
    // network name, nickname
    using NetNick = std::pair<std::string, std::string>;

    ActiveUsers()
      : ignored_nicks{utils::split_string(
            config::data.get_ignore_nicks_buffer(),
            ','
        )}
    {}

    const auto & get_ignored_nicks() const noexcept { return ignored_nicks; }
    const auto & get_data() const noexcept { return data; }

    static const char * get_network() noexcept
        { return hexchat_get_info(ph, "network"); }

    void update(const std::string & nickname) {
        const std::string network = get_network();
        data[{network, nickname}] = std::time(nullptr);
    }

    bool nick_is_ignored(const std::string & nick) const {
        auto first = ignored_nicks.begin();
        auto last = ignored_nicks.end();
        auto pred = [&nick](const std::string & elem) -> bool {
            return hexchat_nickcmp(ph, nick.c_str(), elem.c_str()) == 0;
        };
        return std::find_if(first, last, pred) != last;
    }

    bool
    is_active(const std::string & nickname)
    {
        const std::string network = get_network();
        auto position = data.find({network, nickname});
        if (position == data.end())
            { return false; }

        std::time_t last_seen = position->second;
        std::time_t now = std::time(nullptr);
        if (now < last_seen + config::data.get_user_active_threshold()) {
            return true;
        }
        else {
            data.erase(position);
            return false;
        }
    }

    void rename_if_exists(
        const std::string & old_nick,
        const std::string & new_nick
    ) {
        const std::string network = get_network();
        auto position_old = data.find({network, old_nick});
        if (position_old == data.end())
            { return; }

        auto position_new = data.find({network, new_nick});
        std::time_t seen_at = position_old->second;
        if (position_new != data.end())
            { seen_at = std::max(seen_at, position_new->second); }

        data.insert_or_assign({network, new_nick}, seen_at);
        data.erase(position_old);
    }

private:
    std::map<NetNick, std::time_t> data;
    const std::vector<std::string> ignored_nicks;
};


inline gsl::owner<ActiveUsers *> active_users = nullptr;

#endif  // NOTIFILTER_DATA_H_
