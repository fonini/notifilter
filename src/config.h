#ifndef NOTIFILTER_CONFIG_H_
#define NOTIFILTER_CONFIG_H_

#include <filesystem>
#include <stdexcept>
#include <string>
#include <vector>

#include "cmake_defines.h"


namespace config {

// relative to the user's home directory
constexpr inline const char * default_dir {NOTIFILTER_CONFIG_DEFAULT_DIR};

constexpr inline bool prefer_env {NOTIFILTER_CONFIG_PREFER_ENV};

constexpr inline int default_user_active_threshold_seconds
    {NOTIFILTER_DEFAULT_USER_ACTIVE_THRESHOLD_SECONDS};

constexpr inline int ignore_string_length {1024};


// absolute path to notifilter's config directory
std::filesystem::path dir_path();


class ConfigData {
public:
    ConfigData() = default;

    ConfigData(const ConfigData &) = delete;
    ConfigData & operator =(const ConfigData &) = delete;

    ConfigData(ConfigData &&) = delete;
    ConfigData & operator =(ConfigData &&) = delete;

    ~ConfigData() = default;

    void update();

    bool is_debug() const noexcept
        { return debug; }
    int get_user_active_threshold() const noexcept
        { return user_active_threshold_seconds; }
    const char * get_ignore_nicks_buffer() const noexcept
        { return ignore_nicks; }

private:
    // if the user hasn't been active in the last <value> seconds, then
    // part/join notifications are filtered
    int user_active_threshold_seconds {default_user_active_threshold_seconds};

    // if 'debug' is true, notifications that would be filtered out are
    // instead printed in gray.
    bool debug {false};

    // list of users to ignore (i.e. never filter, always show
    // notifications)
    char ignore_nicks[ignore_string_length] = "";
};

inline ConfigData data {};


class Error: public std::runtime_error {
public:
    Error(const std::string & msg, const std::filesystem::path & p)
      : std::runtime_error{msg}, path{p}
    {}

    const std::filesystem::path & get_path() const noexcept { return path; }

private:
    std::filesystem::path path;
};


class InvalidFilesystemEnvironment: public Error
    { using Error::Error; };


}  // namespace config

#endif  // NOTIFILTER_CONFIG_H_
