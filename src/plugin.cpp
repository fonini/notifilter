#include "plugin.h"

#include <hexchat-plugin.h>

#include "data.h"
#include "notifilter.h"


int channel_message_cb(char ** word, void * /*userdata*/) {
    const char * nickname = word[1];
    if (active_users->nick_is_ignored(nickname))
        { return HEXCHAT_EAT_NONE; }

    active_users->update(nickname);
    return HEXCHAT_EAT_NONE;
}


int quit_cb(char ** word, void * /*userdata*/) {
    const char * nickname = word[1];
    if (active_users->nick_is_ignored(nickname))
        { return HEXCHAT_EAT_NONE; }

    if (active_users->is_active(nickname)) {
        return HEXCHAT_EAT_NONE;
    }
    else if (config::data.is_debug()) {
        hexchat_printf(
            ph,
            "\00315*\017\t\00315(notifilter) %s has Quit\017",
            nickname
        );
        return HEXCHAT_EAT_HEXCHAT;
    }
    else {
        return HEXCHAT_EAT_HEXCHAT;
    }
}


int change_nick_cb(char ** word, void * /*userdata*/) {
    const char * old_nick = word[1];
    const char * new_nick = word[2];
    if (active_users->nick_is_ignored(old_nick)
     || active_users->nick_is_ignored(new_nick))
        { return HEXCHAT_EAT_NONE; }

    active_users->rename_if_exists(old_nick, new_nick);

    if (active_users->is_active(new_nick)) {
        return HEXCHAT_EAT_NONE;
    }
    else if (config::data.is_debug()) {
        hexchat_printf(
            ph,
            "\00315*\017\t\00315(notifilter) %s is now known as %s",
            old_nick,
            new_nick
        );
        return HEXCHAT_EAT_HEXCHAT;
    }
    else {
        return HEXCHAT_EAT_HEXCHAT;
    }
}


int join_cb(char ** word, void * /*userdata*/) {
    const char * nickname = word[1];
    if (active_users->nick_is_ignored(nickname))
        { return HEXCHAT_EAT_NONE; }

    if (active_users->is_active(nickname)) {
        return HEXCHAT_EAT_NONE;
    }
    else if (config::data.is_debug()) {
        hexchat_printf(
            ph,
            "\00315*\017\t\00315(notifilter) %s has joined.",
            nickname
        );
        return HEXCHAT_EAT_HEXCHAT;
    }
    else {
        return HEXCHAT_EAT_HEXCHAT;
    }
}


int part_cb(char ** word, void * /*userdata*/) {
    const char * nickname = word[1];
    if (active_users->nick_is_ignored(nickname))
        { return HEXCHAT_EAT_NONE; }

    if (active_users->is_active(nickname)) {
        return HEXCHAT_EAT_NONE;
    }
    else if (config::data.is_debug()) {
        hexchat_printf(
            ph,
            "\00315*\017\t\00315(notifilter) %s has left.",
            nickname
        );
        return HEXCHAT_EAT_HEXCHAT;
    }
    else {
        return HEXCHAT_EAT_HEXCHAT;
    }
}
