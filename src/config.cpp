#include "config.h"

#include <cstddef>
#include <filesystem>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>

#include "env.h"
#include "io.h"
#include "notifilter.h"

namespace fs = std::filesystem;


namespace {
void string_to_buffer(const std::string & src, char * dst, std::size_t bufsz) {
    const std::size_t string_length = src.size();
    if (bufsz < string_length + 1)
        { throw std::length_error{src}; }
    src.copy(dst, string_length);
    dst[string_length] = '\0';
}
}


namespace config {

fs::path dir_path() {
    std::string env_dir {};
    if constexpr (prefer_env)
        { env_dir = env::get("XDG_CONFIG_HOME"); }

    fs::path path = env_dir.empty() ? default_dir : std::move(env_dir);

    if (path.is_relative())
        { path = fs::path{env::get("HOME")} / path; }

    if (!fs::is_directory(path)) {
        throw InvalidFilesystemEnvironment{
            "The user directory configuration is invalid.",
            path};
    }

    path /= "notifilter";

    if (fs::exists(path)) {
        if (!fs::is_directory(path)) {
            throw InvalidFilesystemEnvironment{
                "The Notifilter configuration directory is invalid.",
                path};
        }
    }
    else {
        fs::create_directory(path);
    }

    return path;
}


void ConfigData::update() {
    fs::path config_file_path = dir_path() / "config";
    if (!fs::exists(config_file_path))
        { return; }
    if (!fs::is_regular_file(config_file_path)) {
        throw InvalidFilesystemEnvironment{
            "Config file should be regular file.",
            config_file_path};
    }

    io::LinesFileReader reader {config_file_path};
    std::string line;

    if (!reader.getline(line))
        { return; }
    user_active_threshold_seconds = std::stoi(line);

    if (!reader.getline(line))
        { return; }
    debug = (line == "debug");

    if (!reader.getline(line))
        { return; }
    string_to_buffer(line, ignore_nicks, ignore_string_length);
}

}  // namespace config
