#ifndef NOTIFILTER_PLUGIN_H_
#define NOTIFILTER_PLUGIN_H_

int
channel_message_cb(char * word[], void * userdata);

int
quit_cb(char * word[], void * userdata);

int
change_nick_cb(char * word[], void * userdata);

int
join_cb(char * word[], void * userdata);

int
part_cb(char * word[], void * userdata);

#endif  // NOTIFILTER_PLUGIN_H
