#ifndef NOTIFILTER_DEBUG_H_
#define NOTIFILTER_DEBUG_H_

int
notifilterdebug_cb(
    char ** /*word*/,
    char ** /*word_eol*/,
    void * /*userdata*/
);

#endif  // NOTIFILTER_DEBUG_H
