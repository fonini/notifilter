#include "io.h"

#include <fstream>
#include <ios>
#include <string>
#include <utility>


namespace io {

LinesFileReader::LinesFileReader(const std::filesystem::path & filepath)
  : input{filepath}
{
    if (!input) {
        throw Error{"io::LinesFileReader::LinesFileReader: file read error."};
    }
    input.exceptions(std::ifstream::badbit);
}

bool LinesFileReader::getchar(char & ch) {
    input.get(ch);
    if (input.fail() && !input.eof())
        { throw std::ios_base::failure{"fail && !eof"}; }
    return !input.eof();
}

bool LinesFileReader::getline(std::string & line) {
    char ch;
    if (!getchar(ch))
        { return false; }

    if (ch == '\n') {
        line.clear();
        return true;
    }

    std::string working_line;

    do { working_line += ch; }
    while (getchar(ch) && ch != '\n');

    line = std::move(working_line);
    return true;
}

}  // namespace io
