#include "notifilter.h"

#include <cstdarg>
#include <cstdio>

#include <hexchat-plugin.h>

#include "data.h"
#include "debug.h"
#include "plugin.h"

extern "C" {
int
hexchat_plugin_init(
    hexchat_plugin * plugin_handle, char ** plugin_name, char ** plugin_desc,
    char ** plugin_version, char * arg
) noexcept;

int
hexchat_plugin_deinit(
    hexchat_plugin *plugin_handle
) noexcept;
}


int set_new_error_msg(const char * fmt...) noexcept {
    std::va_list args;
    va_start(args, fmt);
    int result = std::vsnprintf(
        current_error_msg, sizeof(current_error_msg), fmt, args);
    va_end(args);
    return result;
}


[[gnu::visibility ("default")]]
int
hexchat_plugin_init(
    hexchat_plugin * plugin_handle,
    char ** plugin_name,
    char ** plugin_desc,
    char ** plugin_version,
    char * /*arg*/
) noexcept
{
    ph = plugin_handle;

    *plugin_name = PNAME;
    *plugin_desc = PDESC;
    *plugin_version = PVERSION;

    safe_exec([]{
        config::data.update();
        // NOLINTNEXTLINE(bugprone-unhandled-exception-at-new)
        active_users = new ActiveUsers{};
    });
    if (current_error_msg[0] != '\0') {
        hexchat_printf(
            ph, "Notifilter config ERROR: %s",
            static_cast<char *>(current_error_msg));
        return 0;  // plugin load failure
    }

    hexchat_hook_command(
        ph,
        "notifilterdebug",
        HEXCHAT_PRI_NORM,
        notifilterdebug_cb,
        nullptr,
        nullptr);

    hexchat_hook_print(
        ph,
        "Channel Message",
        HEXCHAT_PRI_NORM,
        channel_message_cb,
        nullptr);

    hexchat_hook_print(
        ph,
        "Quit",
        HEXCHAT_PRI_NORM,
        quit_cb,
        nullptr);

    hexchat_hook_print(
        ph,
        "Change Nick",
        HEXCHAT_PRI_NORM,
        change_nick_cb,
        nullptr);

    hexchat_hook_print(
        ph,
        "Join",
        HEXCHAT_PRI_NORM,
        join_cb,
        nullptr);

    hexchat_hook_print(
        ph,
        "Part",
        HEXCHAT_PRI_NORM,
        part_cb,
        nullptr);

    hexchat_hook_print(
        ph,
        "Part with Reason",
        HEXCHAT_PRI_NORM,
        part_cb,
        nullptr);

    if (config::data.is_debug()) {
        hexchat_print(
            ph, "-->\tInitialized Notifilter");
        hexchat_printf(
            ph, "threshold: %d",
            config::data.get_user_active_threshold());
    }

    return 1;  // success
}


[[gnu::visibility ("default")]]
int
hexchat_plugin_deinit(
    hexchat_plugin * plugin_handle
) noexcept
{
    if (active_users) {
        delete active_users;
        active_users = nullptr;
    }

    if (!plugin_handle && !ph) {
        hexchat_print(nullptr,
            "Unloading Notifilter; unknown error: "
            "both plugin_handle and ph are null.");
        return 0;
    }
    if (!plugin_handle && ph) {
        hexchat_print(ph,
            "Unloading Notifilter; unknown error: plugin_handle is null.");
        return 1;
    }
    // plugin_handle is ok
    if (!ph) {
        hexchat_print(plugin_handle,
            "Unloading Notifilter; unknown error: ph is null.");
        return 0;
    }
    // ph is also ok
    if (ph != plugin_handle) {
        hexchat_print(ph,
            "Unloading Notifilter; unknown error: "
            "ph, you are != plugin_handle.");
        hexchat_print(plugin_handle,
            "Unloading Notifilter; unknown error: "
            "plugin_handle, you are != ph.");
        return 0;
    }
    // ph == plugin_handle != nullptr

    return 1;  // success
}
