#ifndef NOTIFILTER_ENV_H_
#define NOTIFILTER_ENV_H_

#include <cstdlib>

#include <string>

namespace env {

inline std::string get(const char * name) {
    if (const char * contents = std::getenv(name))
        { return {contents}; }
    else
        { return {}; };
}

}  // namespace env

#endif  // NOTIFILTER_ENV_H_
