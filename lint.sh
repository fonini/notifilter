#!/usr/bin/env bash

set -o errexit


CMAKE_LINT_DISABLE=(
    C0103
)


CLANG_TIDY_CHECKS=(
    'bugprone-*'
    -bugprone-easily-swappable-parameters

    'clang-analyzer-*'
    -clang-analyzer-valist.Uninitialized

    'clang-diagnostic-*'
    -clang-diagnostic-ignored-optimization-argument

    'concurrency-*'
    -concurrency-mt-unsafe

    'cppcoreguidelines-*'
    -cppcoreguidelines-avoid-c-arrays
    -cppcoreguidelines-avoid-non-const-global-variables
    -cppcoreguidelines-init-variables
    -cppcoreguidelines-pro-bounds-array-to-pointer-decay
    -cppcoreguidelines-pro-bounds-pointer-arithmetic
    -cppcoreguidelines-pro-type-vararg

    'misc-*'

    'modernize-*'
    -modernize-avoid-c-arrays
    -modernize-pass-by-value
    -modernize-use-nodiscard
    -modernize-use-trailing-return-type

    'performance-*'

    'portability-*'

    'readability-*'
    -readability-else-after-return
    -readability-identifier-length
    -readability-implicit-bool-conversion
)


function print_color {
    local color="$1"
    local text="$2"
    local reset=$'\e[0m'
    printf '%s--> %s%s\n' "$color" "$text" "$reset"
}

function print_err { print_color $'\e[31m' "$1"; }
function print_scc { print_color $'\e[32m' "$1"; }


bad=false
separator="==================================================================="


# SHELL

printf "==> shellcheck"
shellcheck lint.sh \
    || bad=true
printf '\n%s\n\n' "$separator"


# CMAKE

printf "==> cmake-lint\n"
cmake-lint CMakeLists.txt cmake/* --disabled-codes "${CMAKE_LINT_DISABLE[@]}" \
    || bad=true
printf '\n%s\n\n' "$separator"


# C++

printf "==> clang-tidy\n"
checks="$(IFS=,; printf %s "${CLANG_TIDY_CHECKS[*]}")"
clang-tidy -checks="$checks" -header-filter='.*\.h$' src/*.cpp \
    || bad=true
printf '\n%s\n\n' "$separator"

printf "==> cppcheck\n"
cppcheck \
    -I ./build/RelWithDebInfo/include \
    --enable=all --suppress=missingIncludeSystem \
    src \
    || bad=true
printf '\n%s\n\n' "$separator"


# Final

if [[ $bad == true ]]; then
    print_err "SOME CHECKS FAILED"
    exit 1
else
    print_scc "All checks succeeded."
    exit 0
fi
